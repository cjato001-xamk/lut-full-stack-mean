const mongoose = require('mongoose');
const config = require('../config');

mongoose
  .connect(config.MONGODB_URI, {
    dbName: config.MONGODB_DB_NAME,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('MongoDB connected.');
  })
  .catch((error) => {
    console.log(error);
  });

module.exports = mongoose;
