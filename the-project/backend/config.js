const fs = require('fs');

//Development config
const devConfigFile = './config.private.js';
let devConfig = undefined;

//Check development configuration file
try {
  fs.accessSync(devConfigFile, fs.constants.R_OK);

  devConfig = require(devConfigFile);
} catch (error) {
  console.log('Set private configs manually!');
}

//Prioritize environment properties, but if not set, then use locals
const config = {
  MONGODB_URI:
    process.env.MONGODB_URI && process.env.MONGODB_URI !== 0
      ? process.env.MONGODB_URI
      : devConfig.MONGODB_URI,
  MONGODB_DB_NAME: 'lut-mean',
  PORT: process.env.PORT || 6565,
  JWT_SECRET_KEY:
    process.env.JWT_SECRET_KEY && process.env.JWT_SECRET_KEY !== 0
      ? process.env.JWT_SECRET_KEY
      : devConfig.JWT_SECRET_KEY,
  MBOUM_API_KEY:
    process.env.MBOUM_API_KEY && process.env.MBOUM_API_KEY !== 0
      ? process.env.MBOUM_API_KEY
      : devConfig.MBOUM_API_KEY,
};

module.exports = config;
