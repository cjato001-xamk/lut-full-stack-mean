const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const User = require('./models/user');
const config = require('./config');

module.exports = (passport) => {
  passport.use(
    new JwtStrategy(
      {
        jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
        secretOrKey: config.JWT_SECRET_KEY,
      },
      (jwt_payload, done) => {
        User.getUserById(jwt_payload.user._id, (error, user) => {
          if (error) {
            return done(error, false);
          }

          if (user) {
            return done(null, user);
          } else {
            return done(null, false);
          }
        });
      }
    )
  );
};
