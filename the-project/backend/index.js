const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const passport = require('passport');
const path = require('path');

const app = express();

//Helmet
app.use(helmet());

//App settings
const config = require('./config');

//Database connection
const mongoose = require('./database/mongoose');

//Allow requests from frontend during dev (localhost, but different port)
app.use(cors());

//Set static folder (Angular builds frontend to this path)
app.use(express.static(path.join(__dirname, 'public')));

//Enable JSON
app.use(express.json());

//Passport
app.use(passport.initialize());
app.use(passport.session());
require('./passport')(passport);

//Users routes
const users = require('./routes/users');
app.use('/users', users);

//Stocks routes
const stocks = require('./routes/stocks');
app.use('/stocks', stocks);

//All non-defined routes to /
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

//Error handling
app.use((error, req, res, next) => {
  console.log(error);

  res
    .status(500)
    .json({
      success: false,
      message: 'Unknown internal server error occurred, please try again.',
    });
});

//Listen
app.listen(config.PORT, () => {
  console.log(`Server listening on ${config.PORT}`);
});
