const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//User schema
const UserScema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

const User = (module.exports = mongoose.model('User', UserScema));

module.exports.getUserById = (id, callback) => {
  User.findById(id, callback);
};

module.exports.getUserByEmail = (email, callback) => {
  const query = { email: email };

  User.findOne(query, callback);
};

module.exports.addUser = (newUser, callback) => {
  //Convert plain text password to hash and add the user
  bcrypt.genSalt(10, (error, salt) => {
    if (error) {
      throw error;
    }

    bcrypt.hash(newUser.password, salt, (error, hash) => {
      if (error) {
        throw error;
      }

      newUser.password = hash;
      newUser.save(newUser, callback);
    });
  });
};

module.exports.comparePassword = (candidatePassword, hash, callback) => {
  bcrypt.compare(candidatePassword, hash, (error, isMatch) => {
    if (error) {
      throw error;
    }

    callback(null, isMatch);
  });
};
