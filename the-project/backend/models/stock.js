const mongoose = require('mongoose');

//Stock schema
const StockSchema = mongoose.Schema({
  symbol: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  currency: {
    type: String,
  },
  user_ids: {
    type: Array,
  },
});

const Stock = (module.exports = mongoose.model('Stock', StockSchema));

//Get a single stock
module.exports.getStockBySymbol = (symbol, callback) => {
  const query = { symbol: symbol };

  Stock.findOne(query, callback);
};

//Add/update stock to database
module.exports.addOrUpdateStock = (stock, callback) => {
  stock.save(stock, callback);
};

//Add user to stock
module.exports.addUserToStock = (symbol, user_id, callback) => {
  Stock.updateOne(
    { symbol: symbol },
    { $push: { user_ids: user_id } },
    callback
  );
};

//Remove user from stock
module.exports.removeUserFromStock = (symbol, user_id, callback) => {
  Stock.updateOne(
    { symbol: symbol },
    { $pull: { user_ids: mongoose.Types.ObjectId(user_id) } },
    callback
  );
};

//Get users stocks
module.exports.getUsersStocks = (user, callback) => {
  //User's id is in the Stock.user_ids array
  const query = { user_ids: mongoose.Types.ObjectId(user._id) };

  Stock.find(query, callback);
};
