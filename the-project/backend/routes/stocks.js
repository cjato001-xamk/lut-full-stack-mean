const express = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const router = express.Router();
const fetch = require('node-fetch');

const Stock = require('../models/stock');
const config = require('../config');

//Returns all stocks for the user
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res, next) => {
    Stock.getUsersStocks(req.user._id, (error, stocks) => {
      if (error) {
        throw error;
      }

      res.json({
        success: true,
        message: 'Stocks requested.',
        stocks: stocks,
      });
    });
  }
);

//Add a stock to a user
router.post(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res, next) => {
    Stock.getStockBySymbol(req.params.id, (error, stock) => {
      if (error) {
        throw error;
      }

      //Add user to existing stock, or add new stock (and add to user to stock)
      if (stock) {
        //Stock exists but user is not attached to it
        if (!stock.user_ids.includes(req.user._id)) {
          Stock.addUserToStock(req.params.id, req.user._id, (error) => {
            if (error) {
              throw error;
            }

            res.json({
              success: true,
              message: 'Stock "' + req.params.id + '" added.',
            });
          });
        }
      } else {
        //Fetch stock data for the new stock
        fetch(
          'http://mboum.com/api/v1/qu/quote/?symbol=' +
            req.params.id +
            '&apikey=' +
            config.MBOUM_API_KEY
        )
          .then((res) => res.json())
          .then((json) => {
            //Valid stock returned
            if (json.length && json[0].symbol == req.params.id) {
              const stock = new Stock({
                symbol: json[0].symbol,
                name: json[0].longName,
                currency: json[0].currency,
                user_ids: [req.user._id],
              });

              Stock.addOrUpdateStock(stock, (error) => {
                if (error) {
                  throw error;
                }

                res.json({
                  success: true,
                  message: 'Stock "' + req.params.id + '" added.',
                });
              });
            } else {
              return res.json({
                success: false,
                message: 'Failed to add stock "' + req.params.id + '" for you.',
                suggestion:
                  'You should try adding eg. MSFT, AAPL, TSLA, NIO, GME, AMC, BB...',
              });
            }
          })
          .catch((error) => {
            throw error;
          });
      }
    });
  }
);

//Remove a stock from a user
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res, next) => {
    Stock.removeUserFromStock(req.params.id, req.user._id, (error) => {
      if (error) {
        throw error;
      }

      res.json({
        success: true,
        message: 'Stock "' + req.params.id + '" removed.',
      });
    });
  }
);

//Return full data for a single stock
//Also for full backend API update for the stock
router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res, next) => {
    Stock.getStockBySymbol(req.params.id, (error) => {
      if (error) {
        throw error;
      }

      //Fetch full data from the Mboum API
      fetch(
        'http://mboum.com/api/v1/qu/quote/?symbol=' +
          req.params.id +
          '&apikey=' +
          config.MBOUM_API_KEY
      )
        .then((res) => res.json())
        .then((json) => {
          //Valid stock returned
          if (json.length && json[0].symbol == req.params.id) {
            return res.json({
              success: true,
              message: 'Stock "' + req.params.id + '" data.',
              data: json[0],
            });
          } else {
            return res.json({
              success: false,
              message: 'Failed to fetch stock data',
            });
          }
        })
        .catch((error) => {
          throw error;
        });
    });
  }
);

//Get data points for a stock
router.get(
  '/:id/graph/:weeks',
  passport.authenticate('jwt', { session: false }),
  (req, res, next) => {
    //Returns historical data for the past week
    fetch(
      'https://mboum.com/api/v1/hi/history/?symbol=' +
        req.params.id +
        '&interval=1wk&diffandsplits=true&apikey=' +
        config.MBOUM_API_KEY
    )
      .then((res) => res.json())
      .then((json) => {
        //Invalid data
        if (!json.items) {
          return res.json({
            success: false,
            message: 'Failed to fetch graph for "' + req.params.id + '".',
          });
        }

        const weeks = req.params.weeks
          ? Number.isInteger(parseInt(req.params.weeks))
            ? parseInt(req.params.weeks)
            : 1
          : 1;

        //Transform data to datapoints
        const formatted = Object.values(json.items)
          .slice(1)
          .slice(-(weeks * 7))
          .filter((tick) => tick.close !== 0)
          .map((tick) => {
            return { x: tick.date, y: tick.close };
          });

        return res.json({
          success: true,
          message: 'Stock graph for "' + req.params.id + '" fetched.',
          data: formatted,
        });
      })
      .catch((error) => {
        throw error;
      });
  }
);

module.exports = router;
