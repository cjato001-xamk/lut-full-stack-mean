const express = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const router = express.Router();

const User = require('../models/user');
const config = require('../config');

//Register route
router.post('/register', (req, res, next) => {
  let newUser = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
  });

  //Check if email already registered
  User.getUserByEmail(newUser.email, (error, user) => {
    if (error) {
      throw error;
    }

    if (user) {
      return res.json({ success: false, message: 'Email already registered.' });
    }

    //Create new user
    User.addUser(newUser, (error) => {
      if (error) {
        throw error;
      }

      res.json({
        success: true,
        message: 'You are now registered and can login.',
      });
    });
  });
});

//Authentication route
router.post('/authenticate', (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;

  User.getUserByEmail(email, (error, user) => {
    if (error) {
      throw error;
    }

    if (!user) {
      return res.json({
        success: false,
        message: 'Invalid username and/or password.',
      });
    }

    User.comparePassword(password, user.password, (error, isMatch) => {
      if (error) {
        throw error;
      }

      if (isMatch) {
        const token = jwt.sign({ user }, config.JWT_SECRET_KEY, {
          expiresIn: 7 * 24 * 60 * 60,
        });

        return res.json({
          success: true,
          message: 'You are now logged in.',
          token: token,
          user: {
            id: user._id,
            name: user.name,
            email: user.email,
          },
        });
      } else {
        return res.json({
          success: false,
          message: 'Invalid username and/or password.',
        });
      }
    });
  });
});

//Profile route (PROTECTED)
router.get(
  '/profile',
  passport.authenticate('jwt', { session: false }),
  (req, res, next) => {
    const { password, ...sanitizedUser } = req.user._doc;

    //Return user data without sanitized data
    res.json({ user: sanitizedUser });
  }
);

module.exports = router;
