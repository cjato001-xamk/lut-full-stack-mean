import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StockService } from '../../services/stock.service';
import { FlashMessagesService } from 'angular2-flash-messages';

import { IStock, IStockGetStockResponse } from '../../interfaces/stock';

@Component({
  selector: 'app-stock-details',
  templateUrl: './stock-details.component.html',
  styleUrls: ['./stock-details.component.css'],
})
export class StockDetailsComponent implements OnInit {
  symbol: string;
  stock: IStock;

  constructor(
    private route: ActivatedRoute,
    private stockService: StockService,
    private flashMessagesService: FlashMessagesService
  ) {}

  ngOnInit(): void {
    this.symbol = this.route.snapshot.paramMap.get('symbol');

    //Fetch full stock data (backend also does a full stock update)
    this.stockService.getStock(this.symbol).subscribe(
      (response: IStockGetStockResponse) => {
        if (response.success) {
          this.stock = response.data;
        } else {
          this.flashMessagesService.show(response.message, {
            cssClass: 'alert-danger',
            timeout: 15000,
          });
        }
      },
      (error) => {
        this.flashMessagesService.show(
          'Unknown error occured while fetching stock data - please try again.',
          {
            cssClass: 'alert-danger',
            timeout: 15000,
          }
        );
      }
    );
  }
}
