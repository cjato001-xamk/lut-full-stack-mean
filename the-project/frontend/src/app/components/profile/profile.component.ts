import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

import { IUser } from '../../interfaces/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  user: IUser;

  constructor(
    private authService: AuthService,
    private flashMessagesService: FlashMessagesService
  ) {}

  ngOnInit(): void {
    this.authService.getProfile().subscribe(
      (profile: { user: IUser }) => {
        this.user = profile.user;
      },
      (error) => {
        this.flashMessagesService.show(
          'Unknown error occurred while fetching your profile - please try again.',
          {
            cssClass: 'alert-danger',
            timeout: 5000,
          }
        );

        return false;
      }
    );
  }
}
