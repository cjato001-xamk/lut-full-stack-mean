import { Component, Input, OnInit } from '@angular/core';
import { StockService } from '../../services/stock.service';
import * as CanvasJS from '../../../assets/canvasjs.min';
import { FlashMessagesService } from 'angular2-flash-messages';

import { IStock, IStockGraphResponse } from '../../interfaces/stock';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css'],
})
export class GraphComponent implements OnInit {
  @Input() stock: IStock;

  @Input() weeks: number;

  loading: boolean = false;

  constructor(
    private stockService: StockService,
    private flashMessagesService: FlashMessagesService
  ) {}

  ngOnInit(): void {
    this.chartMe(this.stock.symbol, this.weeks);
  }

  chartMe(symbol: string, weeks: number) {
    this.loading = true;

    this.stockService.getGraph(symbol, weeks).subscribe(
      (response: IStockGraphResponse) => {
        if (response.success) {
          this.loading = false;

          //Not sure why this "timeout" of 0 is needed,
          //but does not work without this
          setTimeout(() => {
            let chart = new CanvasJS.Chart(symbol + '-chartContainer', {
              title: {
                text: symbol,
              },
              axisX: {
                interval: 1,
                intervalType: 'week',
              },
              backgroundColor: '#f8f9fa',
              data: [
                {
                  type: 'stepLine',
                  dataPoints: response.data.map(
                    (tick: { x: string; y: number }) => {
                      //Fugly datehandling, but moment or alike would be an overkill
                      return {
                        x: new Date(
                          parseInt(tick.x.split('-')[2]),
                          parseInt(tick.x.split('-')[1]),
                          parseInt(tick.x.split('-')[0])
                        ),
                        y: tick.y,
                      };
                    }
                  ),
                },
              ],
            });

            chart.render();
          }, 0);
        } else {
          this.flashMessagesService.show(response.message, {
            cssClass: 'alert-danger',
            timeout: 5000,
          });
        }
      },
      (error) => {
        this.flashMessagesService.show(
          'Unknown error in the graph component.',
          {
            cssClass: 'alert-danger',
            timeout: 5000,
          }
        );
      }
    );
  }
}
