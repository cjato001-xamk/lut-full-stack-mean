import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

import { IUser, IUserResponse } from '../../interfaces/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  name: string;
  email: string;
  password: string;

  constructor(
    private validateService: ValidateService,
    private authService: AuthService,
    private flashMessagesService: FlashMessagesService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onRegisterSubmit() {
    const user: IUser = {
      name: this.name,
      email: this.email,
      password: this.password,
    };

    //Validate required fields
    if (!this.validateService.validateRegister(user)) {
      this.flashMessagesService.show('Please fill in all fields!', {
        cssClass: 'alert-danger',
        timeout: 5000,
      });

      return false;
    }

    //Validate email
    if (!this.validateService.validateEmail(user.email)) {
      this.flashMessagesService.show('Please fill in valid email!', {
        cssClass: 'alert-danger',
        timeout: 5000,
      });

      return false;
    }

    //Register user
    this.authService.registerUser(user).subscribe((response: IUserResponse) => {
      if (response.success) {
        this.flashMessagesService.show(response.message, {
          cssClass: 'alert-success',
          timeout: 5000,
        });

        //Pass along the email and prefill that in the login form
        this.router.navigate(['login'], {
          state: { data: { email: user.email } },
        });
      } else {
        this.flashMessagesService.show(response.message, {
          cssClass: 'alert-danger',
          timeout: 5000,
        });
      }
    });
  }
}
