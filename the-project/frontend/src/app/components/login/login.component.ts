import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

import { IUser, IUserResponse } from '../../interfaces/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;

  constructor(
    private router: Router,
    private authService: AuthService,
    private flashMessagesService: FlashMessagesService
  ) {}

  ngOnInit(): void {
    //This email gets passed from the register page
    if (history.state.data && history.state.data.email) {
      this.email = history.state.data.email;
    }
  }

  onLoginSubmit() {
    const user: IUser = {
      email: this.email,
      password: this.password,
      name: undefined,
    };

    //Login user
    this.authService
      .authenticateUser(user)
      .subscribe((response: IUserResponse) => {
        if (response.success) {
          this.authService.storeUserData({
            token: response.token,
            user: response.user,
          });

          this.flashMessagesService.show(response.message, {
            cssClass: 'alert-success',
            timeout: 5000,
          });

          this.router.navigate(['dashboard']);
        } else {
          this.flashMessagesService.show(response.message, {
            cssClass: 'alert-danger',
            timeout: 5000,
          });
        }
      });
  }
}
