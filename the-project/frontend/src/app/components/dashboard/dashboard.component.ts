import { Component, OnInit, ViewChild } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';

import { StocksComponent } from '../stocks/stocks.component';
import { StockService } from '../../services/stock.service';

import { IStock, IStockAddResponse } from '../../interfaces/stock';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  symbol: string;

  @ViewChild(StocksComponent) child: StocksComponent;

  constructor(
    private stockService: StockService,
    private flashMessagesService: FlashMessagesService
  ) {}

  ngOnInit(): void {}

  onAddNewStockSubmit() {
    //Requested stock already in user's stocks
    if (
      this.child.stocks.filter((stock: IStock) => stock.symbol === this.symbol)
        .length
    ) {
      this.flashMessagesService.show(
        'You already have "' + this.symbol + '".',
        {
          cssClass: 'alert-info',
          timeout: 5000,
        }
      );

      return false;
    }

    //Shortest valid symbols are 1 chars (eg. F)
    if (this.symbol.length) {
      this.stockService.addStock(this.symbol).subscribe(
        (response: IStockAddResponse) => {
          if (response.success) {
            this.flashMessagesService.show(response.message, {
              cssClass: 'alert-success',
              timeout: 5000,
            });

            //Re-render StocksComponent
            this.child.getStocks();
          } else {
            this.flashMessagesService.show(response.message, {
              cssClass: 'alert-danger',
              timeout: 8000,
            });

            if (response.suggestion) {
              this.flashMessagesService.show(response.suggestion, {
                cssClass: 'alert-info',
                timeout: 5000,
              });
            }
          }
        },
        (error) => {
          this.flashMessagesService.show(
            'Unknown error occurred while adding a stock - please try again.',
            {
              cssClass: 'alert-danger',
              timeout: 8000,
            }
          );

          return false;
        }
      );
    } else {
      this.flashMessagesService.show('Type something before submitting...', {
        cssClass: 'alert-warning',
        timeout: 5000,
      });
    }
  }
}
