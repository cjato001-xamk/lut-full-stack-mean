import { Component, OnInit } from '@angular/core';
import { StockService } from '../../services/stock.service';
import { FlashMessagesService } from 'angular2-flash-messages';

import {
  IStock,
  IStockRemoveResponse,
  IStockGetStocksResponse,
} from '../../interfaces/stock';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css'],
})
export class StocksComponent implements OnInit {
  stocks: IStock[];

  constructor(
    private stockService: StockService,
    private flashMessagesService: FlashMessagesService
  ) {}

  ngOnInit(): void {
    this.getStocks();
  }

  getStocks() {
    this.stockService.getStocks().subscribe(
      (stocks: IStockGetStocksResponse) => {
        this.stocks = stocks.stocks;
      },
      (error) => {
        this.flashMessagesService.show(
          'Unknown error occurred while fetching stocks.',
          {
            cssClass: 'alert-success',
            timeout: 5000,
          }
        );

        return false;
      }
    );
  }

  onRemoveStockClick(symbol: string) {
    this.stockService.removeStock(symbol).subscribe(
      (response: IStockRemoveResponse) => {
        this.flashMessagesService.show(symbol + ' removed', {
          cssClass: 'alert-success',
          timeout: 5000,
        });

        this.stocks = this.stocks.filter((stock) => symbol !== stock.symbol);
      },
      (error) => {
        this.flashMessagesService.show(
          'Unknown error occurred while removing stock.',
          {
            cssClass: 'alert-danger',
            timeout: 5000,
          }
        );

        return false;
      }
    );
  }
}
