import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

import { IUser } from '../../interfaces/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  user: IUser;

  constructor(
    public authService: AuthService,
    private flashMessagesService: FlashMessagesService
  ) {}

  ngOnInit(): void {
    if (this.authService.loggedIn()) {
      this.authService.getProfile().subscribe(
        (profile: { user: IUser }) => {
          this.user = profile.user;
        },
        (error) => {
          this.flashMessagesService.show(
            'Unknown error while logging in - please try again.',
            {
              cssClass: 'alert-danger',
              timeout: 5000,
            }
          );

          return false;
        }
      );
    }
  }
}
