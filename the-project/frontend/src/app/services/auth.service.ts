import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

import { environment } from '../../environments/environment';

import { IUser } from '../interfaces/user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user: IUser;

  constructor(private http: HttpClient, public jwtHelper: JwtHelperService) {}

  registerUser(user: IUser) {
    const headers = { 'Content-Type': 'application/json' };

    return this.http
      .post(environment.apiRoot + '/users/register', user, { headers: headers })
      .pipe(map((res) => res));
  }

  //Login user
  authenticateUser(user: IUser) {
    const headers = { 'Content-Type': 'application/json' };

    return this.http
      .post(environment.apiRoot + '/users/authenticate', user, {
        headers: headers,
      })
      .pipe(map((res) => res));
  }

  getProfile() {
    const headers = {
      'Content-Type': 'application/json',
    };

    return this.http
      .get(environment.apiRoot + '/users/profile', {
        headers: headers,
      })
      .pipe(map((res) => res));
  }

  //Returns true if token is not expired
  loggedIn() {
    return !this.jwtHelper.isTokenExpired();
  }

  //Store user data to local storage
  storeUserData(data: { token: string; user: IUser }) {
    localStorage.setItem('id_token', data.token);
    localStorage.setItem('user', JSON.stringify(data.user));

    this.user = data.user;
  }

  logoutUser() {
    localStorage.clear();

    this.user = null;
  }
}
