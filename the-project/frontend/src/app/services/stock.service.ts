import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class StockService {
  constructor(private http: HttpClient) {}

  addStock(symbol: string) {
    const headers = { 'Content-Type': 'application/json' };

    return this.http
      .post(environment.apiRoot + '/stocks/' + symbol, {
        headers: headers,
      })
      .pipe(map((res) => res));
  }

  removeStock(symbol: string) {
    const headers = { 'Content-Type': 'application/json' };

    return this.http
      .delete(environment.apiRoot + '/stocks/' + symbol, {
        headers: headers,
      })
      .pipe(map((res) => res));
  }

  getStock(symbol: string) {
    const headers = { 'Content-Type': 'application/json' };

    return this.http
      .get(environment.apiRoot + '/stocks/' + symbol, {
        headers: headers,
      })
      .pipe(map((res) => res));
  }

  getStocks() {
    const headers = { 'Content-Type': 'application/json' };

    return this.http
      .get(environment.apiRoot + '/stocks', {
        headers: headers,
      })
      .pipe(map((res) => res));
  }

  getGraph(symbol: string, weeks: number) {
    const headers = { 'Content-Type': 'application/json' };

    return this.http
      .get(environment.apiRoot + '/stocks/' + symbol + '/graph/' + weeks, {
        headers: headers,
      })
      .pipe(map((res) => res));
  }
}
