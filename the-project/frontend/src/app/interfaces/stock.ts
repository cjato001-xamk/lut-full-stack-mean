export interface IStock {
  symbol: string;
  user_ids: string[];
  _id: string;
  name: string;
}

export interface IStockAddResponse {
  success: boolean;
  message: string;
  suggestion?: string;
}

export interface IStockRemoveResponse {
  message: string;
  success: boolean;
}

export interface IStockGraphResponse {
  success: boolean;
  message: string;
  data: object[];
}

export interface IStockGetStockResponse {
  success: boolean;
  message: string;
  data?: IStock;
}

export interface IStockGetStocksResponse {
  message: string;
  success: boolean;
  stocks: IStock[];
}
