export interface IUser {
  name: string;
  email: string;
  password: string;
}

export interface IUserResponse {
  success: boolean;
  message: string;
  token?: string;
  user?: IUser;
}
