# Running the app locally

```
cd ./the-project/backend
code config.js
- set MONGODB_URI
- set MBOUM_API_KEY (API key can be obtained from https://mboum.com/, it is free to register, but has extremely annoying limit of 100 requests/month/API KEY - I burned a lot of users while making this)
npm start
```

App opens at http://localhost:6565

The frontend has been built `ng build --prod` into `./backend/public`, so it's only needed to start the backend server.

...or you can just go to Heroku and see the app up and running there...
https://floating-atoll-27072.herokuapp.com/

Limit of 100 requests/month/API KEY to the Mboum API applies in Heroku as well. Upon deployment to Heroku I did set a fresh key, but someone might use that as this is indeed public... Please don't - I need this project graded. :)

# Build/deploy process

## Initial deployment to Heroku

```
git add .
git commit -m ''

cd 'git repo root'
heroku create
(login to Heroku if needed)
heroku git:remote -a floating-atoll-27072
git subtree push --prefix the-project/backend heroku master
```

Set private config variables, eg. in the Heroku Dashboard

## App updates to Heroku

... Do `ng build --prod` in frontend -> creates frontend files to ./the-project/backend/public

```
git add .
git commit -m '...'
cd 'git repo root'
git subtree push --prefix the-project/backend heroku master
```

# Video of the running app on Heroku (no audio)
![Stocksboard demo](stocksboard-demo.mov)

# The Project Rules

Some ground rules which I've set for myself:

1. [x] Use MongoDB Atlas
2. [x] Fully running App in Heroku
3. [x] Solid enough job so that I can leave the app running and it might actually be of some use to someone
4. [x] npm i helmet and implement

Plus what the series defined:

1. [x] REST API (Express.js)
2. [x] Tokens and auth
3. [x] CORS
4. [x] Mongoose
5. [x] Angular
6. [x] JWT
7. [x] Protected routes
8. [x] Protected requests

## The Project Planning

This is annoying... As I have no idea what to build I can't get started from any direction. Can't do backend when I have no idea of what the backend is serving and can't do the frontend...

### Idea 1: "Learn consolidator"?

- Helps to keep track of courses from multiple different Moodle-platforms, etc.
- Allows to CRUD courses
- Courses can have start/end
- Course has ECTS, which defines the weekly target effort for the course and this can be divided to some timeline
- It's possible to give own estimate of the actual hours which one thinks is needed for the course
- It's possible to state the progress of the given course
- App would suggest which course should be focused on - based on simple math
- And as a bonus - each course will have an image of a cuddly animal, which is fetched from eg. Pixabay

### Idea 2: "Twitter aggregator - All the presidents"

- Aggregates tweets from the select presidents

### Idea 3: "Something else - currently my frontend app is named as 'Frontend for something...'"

- ...

### Idea 4: "Stocks dashboard"

- I know - done many times... But would have external API usage, data fetching/updating, etc. - "This could be the one..."
- Features:
  - Possibility to add tickers
    - Form with single input
    - Add-button
    - Error if invalid ticker
    - Add ticker to users profile if found
    - Add ticker to dashboard-view live if found
    - Saves couple of hours of ticker data to database and renders a simple line graph from this
  - Possibility to remove tickers
- This would satisfy "Be some use", ie. my "Ground rule 3" and not just being "some project"
- Possible external API's
  - https://finnhub.io/
  - https://mboum.com/api/documentation#quotes
    - Nice looking API
  - https://www.alphavantage.co/documentation/
    - The API looks pretty horrible at a glance
      `{ "Global Quote": { "01. symbol": "IBM", "02. open": "119.1800", "03. high": "122.5900", "04. low": "118.4200", "05. price": "122.4900", "06. volume": "10694708", "07. latest trading day": "2021-01-26", "08. previous close": "118.5800", "09. change": "3.9100", "10. change percent": "3.2974%" } }`
