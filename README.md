# lut-full-stack-mean

In your git repository there should be the following:

1. [x] [Material from exercise projects](exercise-projects/)
2. [x] [Learning Diary](learning-diary.docx)
3. [x] The Project - [Stocksboard](the-project/)
4. [x] [README](the-project/README.md) on how to run the project
5. [x] Video is in [/the-project/stocksboard-demo.mov](the-project/stocksboard-demo.mov) and is embedded to project's [README](the-project/README.md)
