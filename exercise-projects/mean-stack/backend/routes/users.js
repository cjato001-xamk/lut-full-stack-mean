const express = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const router = express.Router();

const User = require('../models/user');
const config = require('../config');

//Register route
router.post('/register', (req, res, next) => {
  let newUser = new User({
    name: req.body.name,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
  });

  User.addUser(newUser, (error, user) => {
    if (error) {
      return res.json({ success: false, message: 'Failed to register user.' });
    }

    res.json({ success: true, message: 'User registered.' });
  });
});

//Authentication route
router.post('/authenticate', (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;

  User.getUserByEmail(email, (error, user) => {
    if (error) {
      throw error;
    }

    if (!user) {
      return res.json({ success: false, message: 'User not found.' });
    }

    User.comparePassword(password, user.password, (error, isMatch) => {
      if (error) {
        throw error;
      }

      if (isMatch) {
        const token = jwt.sign({ user }, config.JWT_SECRET_KEY, {
          expiresIn: 7 * 24 * 60 * 60,
        });

        return res.json({
          success: true,
          token: 'JWT ' + token,
          user: {
            id: user._id,
            name: user.name,
            email: user.email,
            username: user.username,
          },
        });
      } else {
        return res.json({ success: false, message: 'Invalid password.' });
      }
    });
  });
});

//PROTECTED Profile route
router.get(
  '/profile',
  passport.authenticate('jwt', { session: false }),
  (req, res, next) => {
    console.log('getting profile');
    res.json({ user: req.user });
  }
);

module.exports = router;
