const express = require('express');
const cors = require('cors');
const passport = require('passport');
const path = require('path');
//const logger = require('../../exercise-projects/express.js/middleware/logger');
const app = express();

//Init logger middleware
//app.use(logger);

//App settings
const config = require('./config');

//Database connection
const mongoose = require('./database/mongoose');

//Allow requests from frontend (local, but different port)
app.use(cors());

//Set static folder
app.use(express.static(path.join(__dirname, 'public')));

//Enable JSON
app.use(express.json());

//Passport
app.use(passport.initialize());
app.use(passport.session());
require('./passport')(passport);

//Users route
const users = require('./routes/users');
app.use('/users', users);

//FIXME: Temporary / route
app.get('/', (req, res) => {
  res.send('Invalid endpoint');
});

//All non-defined routes to /
//FIXME: This is not going be /public? in the end
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

const PORT = process.env.PORT || 6565;

//Listen
app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
