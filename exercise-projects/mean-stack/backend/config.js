const fs = require('fs');

//Development config
const devConfigFile = './config.private.js';
let devConfig = undefined;

//Check development configuration file
try {
  fs.accessSync(devConfigFile, fs.constants.R_OK);

  devConfig = require(devConfigFile);
} catch (error) {
  console.log('Set private configs manually!');
}

//Prioritize environment properties, but if not set, then use locals
const config = {
  MONGODB_URI:
    process.env.MONGODB_URI && process.env.MONGODB_URI !== 0
      ? process.env.MONGODB_URI
      : devConfig.MONGODB_URI,
  JWT_SECRET_KEY:
    process.env.JWT_SECRET_KEY && process.env.JWT_SECRET_KEY !== 0
      ? process.env.JWT_SECRET_KEY
      : devConfig.JWT_SECRET_KEY,
};

module.exports = config;
