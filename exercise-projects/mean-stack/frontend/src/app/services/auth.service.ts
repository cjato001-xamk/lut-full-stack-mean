import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  authToken: string; //FIXME: Can remove from here? -jwt handles this
  user: any;

  constructor(private http: HttpClient, public jwtHelper: JwtHelperService) {}

  registerUser(user) {
    const headers = { 'Content-Type': 'application/json' };

    return this.http
      .post('/users/register', user, { headers: headers })
      .pipe(map((res) => res));
  }

  authenticateUser(user) {
    const headers = { 'Content-Type': 'application/json' };

    return this.http
      .post('/users/authenticate', user, {
        headers: headers,
      })
      .pipe(map((res) => res));
  }

  //TODO: Could also just use the data in the local
  //storage as it's the exact same data
  getProfile() {
    const headers = {
      'Content-Type': 'application/json',
      //Authorization: this.authToken,
      Authorization: localStorage.getItem('id_token'), //FIXME: This can be deprecated
    };

    return this.http
      .get('/users/profile', {
        headers: headers,
      })
      .pipe(
        map((res) => {
          console.log(res);
          return res;
        })
      );
  }

  loggedIn() {
    return !this.jwtHelper.isTokenExpired();
  }

  //Store user data to local storage
  storeUserData(data: { token: string; user: any }) {
    localStorage.setItem('id_token', data.token);
    localStorage.setItem('user', JSON.stringify(data.user));

    this.authToken = data.token;
    this.user = data.user;
  }

  logoutUser() {
    localStorage.clear();

    this.authToken = null;
    this.user = null;
  }
}
