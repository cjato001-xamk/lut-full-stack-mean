import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  user: Object;

  constructor(
    private authService: AuthService,
    private flashMessageService: FlashMessagesService,
    public jwtHelper: JwtHelperService
  ) {}

  ngOnInit(): void {
    console.log(
      'Profile - jwt token expired?',
      this.jwtHelper.isTokenExpired()
    ); // true or false

    console.log('token expires at', this.jwtHelper.getTokenExpirationDate()); // date

    this.authService.getProfile().subscribe(
      (profile: { user: any }) => {
        this.user = profile.user;
      },
      (error) => {
        console.error(error);

        this.flashMessageService.show(error, {
          cssClass: 'alert-danger',
          timeout: 5000,
        });

        return false;
      }
    );
  }
}
