import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  email: String;
  password: String;

  constructor(
    private router: Router,
    private authService: AuthService,
    private flashMessageService: FlashMessagesService
  ) {}

  ngOnInit(): void {
    //This email gets passed from the register page
    if (history.state.data && history.state.data.email) {
      this.email = history.state.data.email;
    }
  }

  onLoginSubmit() {
    const user = {
      email: this.email,
      password: this.password,
    };

    //Login user
    this.authService
      .authenticateUser(user)
      .subscribe(
        (data: {
          message: String;
          success: boolean;
          token: string;
          user: any;
        }) => {
          if (data.success) {
            this.authService.storeUserData({
              token: data.token,
              user: data.user,
            });

            this.flashMessageService.show('You are now logged in.', {
              cssClass: 'alert-success',
              timeout: 5000,
            });

            this.router.navigate(['dashboard']);
          } else {
            this.flashMessageService.show(
              'Failed to login, because "' + data.message + '"!',
              {
                cssClass: 'alert-danger',
                timeout: 5000,
              }
            );
          }
        }
      );
  }
}
