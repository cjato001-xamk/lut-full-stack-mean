import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  name: String;
  username: String;
  email: String;
  password: String;

  constructor(
    private validateService: ValidateService,
    private authService: AuthService,
    private flashMessageService: FlashMessagesService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onRegisterSubmit() {
    const user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password,
    };

    //Validate required fields
    if (!this.validateService.validateRegister(user)) {
      this.flashMessageService.show('Please fill in all fields!', {
        cssClass: 'alert-danger',
        timeout: 5000,
      });

      return false;
    }

    //Validate email
    if (!this.validateService.validateEmail(user.email)) {
      this.flashMessageService.show('Please fill in valid email!', {
        cssClass: 'alert-danger',
        timeout: 5000,
      });

      return false;
    }

    //Register user
    this.authService
      .registerUser(user)
      .subscribe((data: { message: String; success: boolean }) => {
        if (data.success) {
          //This message is technically shown after router.navigate in the /login-component
          this.flashMessageService.show(
            'You are now registered and can login.',
            {
              cssClass: 'alert-success',
              timeout: 5000,
            }
          );

          //Pass along the email and prefill that in the login form
          this.router.navigate(['login'], {
            state: { data: { email: user.email } },
          });
        } else {
          this.flashMessageService.show(
            'Failed to register, because "' + data.message + '"!',
            {
              cssClass: 'alert-danger',
              timeout: 5000,
            }
          );
        }
      });
  }
}
