const moment = require('moment');

const logger = (req, res, next) => {
  console.log(
    `${req.protocol}://${req.get('host')}${
      req.originalUrl
    }:${moment().format()}`
  );

  if (req.originalUrl.indexOf('profile')) {
    console.log(req.headers);
  }

  next();
};

module.exports = logger;
