const express = require('express');
const router = express.Router();
const uuid = require('uuid');

const members = require('../../Members');

//Get all members
router.get('/', (req, res) => {
  res.json(members);
});

//Get single member
router.get('/:id', (req, res) => {
  const found = members.some((x) => x.id == req.params.id);

  if (found) {
    res.json(members.filter((x) => x.id == req.params.id));
  } else {
    res.status(400).json({ msg: `Member with id ${req.params.id} not found` });
  }
});

//Create member
router.post('/', (req, res) => {
  const newMember = {
    id: uuid.v4(),
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    status: 'active',
  };

  if (!newMember.email || !newMember.password) {
    return res.status(400).json({ msg: 'Please include email and password!' });
  }

  members.push(newMember);

  console.log(newMember);
  console.log(members);

  //res.json(members);
  res.redirect('/');
});

//Update member
router.put('/:id', (req, res) => {
  const found = members.some((x) => x.id == req.params.id);

  if (found) {
    const updateMember = req.body;

    members.forEach((member) => {
      if (member.id == req.params.id) {
        if (updateMember.email) {
          member.email = req.body.email;
        }

        if (updateMember.password) {
          member.password = req.body.password;
        }
      }

      res.json({ msg: 'Member updated', member: member });
    });
  } else {
    res.status(400).json({ msg: `Member with id ${req.params.id} not found` });
  }
});

//Delete single member
router.delete('/:id', (req, res) => {
  const found = members.some((x) => x.id == req.params.id);

  if (found) {
    res.json({
      msg: 'Member deleted!',
      members: members.filter((x) => x.id != req.params.id),
    });
  } else {
    res.status(400).json({ msg: `Member with id ${req.params.id} not found` });
  }
});

module.exports = router;
