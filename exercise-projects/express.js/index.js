const express = require('express');
const path = require('path');
const logger = require('./middleware/logger');
const hbs = require('express-handlebars');

const members = require('./Members');

const app = express();

//Handlebars middleware
app.engine('.hbs', hbs({ extname: '.hbs' }));
app.set('view engine', '.hbs');

//Init logger middleware
app.use(logger);

//Body parser middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//Home page route
app.get('/', (req, res) => {
  res.render('index', {
    title: 'Member app',
    members: members,
  });
});

//Set static folder
app.use(express.static(path.join(__dirname, 'public')));

//Members api routes
app.use('/api/members', require('./routes/api/members'));

const PORT = process.env.PORT || 5001;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
