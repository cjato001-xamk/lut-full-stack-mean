const members = [
  { id: 1, name: 'Peter', email: 'peter@gmail.com', password: 'p3ter' },
  { id: 2, name: 'John', email: 'john@gmail.com', password: 'j0hn' },
  { id: 3, name: 'Martin', email: 'martin@gmail.com', password: 'm4rtin' },
];

module.exports = members;
