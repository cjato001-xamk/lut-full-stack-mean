const url = require('url');

const myUrl = new URL(
  'http://mywebsite.com:8181/hello.html?id=110&status=active'
);

//Serialized URL
console.log(myUrl.href);

//Host
console.log(myUrl.host);

//Hostname
console.log(myUrl.hostname);

//Pathname
console.log(myUrl.pathname);

//serialized query
console.log(myUrl.search);

//Params object
console.log(myUrl.searchParams);

//Add params
myUrl.searchParams.append('abc', 123);
console.log(myUrl.searchParams);

//Loop through params
myUrl.searchParams.forEach((value, name) => {
  console.log(`${name}: ${value}`);
});
