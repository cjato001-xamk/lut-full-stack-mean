const http = require('http');

//Create a server object
http
  .createServer((request, response) => {
    //Write response
    response.write('Hello to U2');
    response.end();
  })
  .listen(5000, () => {
    console.log('Server running!');
  });
