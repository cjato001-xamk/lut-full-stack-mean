const fs = require('fs');
const path = require('path');

//Create folder
if (!fs.existsSync(path.join(__dirname, '/test'))) {
  fs.mkdir(path.join(__dirname, '/test'), {}, (error) => {
    if (error) {
      throw error;
    }

    console.log('Folder created...');
  });
}

//Create and write to file
fs.writeFile(
  path.join(__dirname, '/test', 'hello.txt'),
  'I should be in the file!',
  (error) => {
    if (error) {
      throw error;
    }

    console.log('File written to...');

    //Append to file
    fs.appendFile(
      path.join(__dirname, '/test', 'hello.txt'),
      ' I came in second!',
      (error) => {
        if (error) {
          throw error;
        }

        console.log('Appended to file...');

        //Check file exists
        fs.access(
          path.join(__dirname, '/test', 'hello.txt'),
          fs.F_OK,
          (error) => {
            if (error) {
              throw error;
            }

            //Read file
            fs.readFile(
              path.join(__dirname, '/test', 'hello.txt'),
              'utf8',
              (error, data) => {
                if (error) {
                  throw error;
                }

                console.log('Data from the file: ', data);

                fs.rename(
                  path.join(__dirname, '/test', 'hello.txt'),
                  path.join(__dirname, '/test', 'hello-renamed.txt'),
                  (error) => {
                    if (error) {
                      throw error;
                    }

                    console.log('File renamed...');
                  }
                );
              }
            );
          }
        );
      }
    );
  }
);
