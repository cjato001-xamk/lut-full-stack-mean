const path = require('path');

//Base file name
console.log('base', path.basename(__filename));

//Directory name
console.log('dir', path.dirname(__filename));

//File extension
console.log('ext', path.extname(__filename));

//Create path object
console.log('obj', path.parse(__filename).base);

//Concatenate paths
console.log('concat', path.join(__dirname, 'test', 'hello.html'));
