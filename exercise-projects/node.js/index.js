const http = require('http');
const path = require('path');
const fs = require('fs');

const server = http.createServer((req, res) => {
  //Build file path
  let filePath = path.join(
    __dirname,
    'public',
    req.url === '/' ? 'index.html' : req.url
  );

  //Initial content type
  let contentType = 'text/html';

  //Check extension -> set content type
  switch (path.extname(filePath)) {
    case '.js':
      contentType = 'text/javascript';
      break;

    case '.css':
      contentType = 'text/css';
      break;

    case '.json':
      contentType = 'application/json';
      break;

    case '.png':
      contentType = 'image/png';
      break;

    case '.jpg':
      contentType = 'image/jpg';
      break;

    case '':
      filePath += '.html';
  }

  fs.readFile(filePath, (error, content) => {
    if (error) {
      //Page not found
      if (error.code == 'ENOENT') {
        fs.readFile(
          path.join(__dirname, 'public', '404.html'),
          (error, content) => {
            res.writeHead(200, { 'Content-Type': 'text/html' });

            res.end(content, 'utf8');
          }
        );
      } else {
        //Some server error
        res.writeHead(500);
        res.end(`Server error ${error.code}`);
      }
    } else {
      //Success
      res.writeHead(200, { 'Content-Type': contentType });

      res.end(content, 'utf8');
    }
  });

  // if (req.url === '/') {
  //   fs.readFile(path.join(__dirname, 'public', 'index.html'), (error, data) => {
  //     if (error) {
  //       throw error;
  //     }

  //     res.writeHead(200, { 'Content-Type': 'text/html' });

  //     res.end(data);
  //   });
  // } else if (req.url === '/about') {
  //   fs.readFile(path.join(__dirname, 'public', 'about.html'), (error, data) => {
  //     if (error) {
  //       throw error;
  //     }

  //     res.writeHead(200, { 'Content-Type': 'text/html' });

  //     res.end(data);
  //   });
  // } else if (req.url === '/api/users') {
  //   const users = [
  //     { name: 'Me', age: 36 },
  //     { name: 'Others', age: 40 },
  //   ];

  //   res.writeHead(200, { 'Content-Type': 'application/json' });
  //   res.end(JSON.stringify(users));
  // } else {
  //   res.end(JSON.stringify({ error: { message: 'unknown path' } }));
  // }
});

const PORT = process.env.PORT || 5000;

server.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
